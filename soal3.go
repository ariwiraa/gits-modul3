package main

import (
	"fmt"
)

//var isPrima int

func main() {

	var b int
	var i int

	fmt.Printf("\n->Silahkan ketik angka awal: \n") //mulai dari angka terkecil
	fmt.Scan(&b)

	fmt.Printf("->Silahkan ketik angka akhir: \n") //sampai angka terbesar
	fmt.Scan(&i)

	fmt.Print("Mulai Angka ", b, " Sampai ", i)

	if ((b > 0) && (i > 0)) || (b < i) {
		fmt.Print("\nResult-> Deret bilangan : \n\t\t")
		for j := b; j <= i; j++ {
			fmt.Print(j, " ")
			if j%10 == 0 {
				fmt.Printf("\n\t\t")
			}
		}
	}
	genap(b, i)
	ganjil(b, i)
}

func genap(b, i int) int {
	var y int
	fmt.Print("Deretan Genap->")
	for y = b; y <= i; y++ {
		if y%2 == 0 {
			fmt.Print(y, ",")

		}
	}
	return y
}

func ganjil(b, i int) int {
	var y int
	fmt.Println("")
	fmt.Print("Deretan Ganjil->")
	for y = b; y <= i; y++ {
		if y%2 == 1 {
			fmt.Print(y, ",")

		}
	}
	return y
}

//end syntax-nya
