package main

import "fmt"

func main() {
	var panjang, lebar int

	fmt.Println("Masukan nilai panjang :")
	fmt.Scan(&panjang)

	fmt.Println("Masukan nilai lebar :")
	fmt.Scan(&lebar)

	luas := panjang * lebar
	keliling := 2 * (panjang + lebar)

	fmt.Println("Luas =", luas)
	fmt.Println("Keliling =", keliling)
}
