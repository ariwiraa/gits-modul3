package main

import "fmt"

func main() {
	var word string

	fmt.Println("Masukan Kata :")
	fmt.Scan(&word)

	fmt.Printf("Kata yang dimasukan : %s\n", word)
	fmt.Println("Panjang kata :", len(word), "karakter")

}
