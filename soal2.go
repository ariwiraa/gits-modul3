package main

import "fmt"

func main() {
	var cel int
	var fahrenheit, kelvin float64

	fmt.Println("Masukan celcius :")
	fmt.Scan(&cel)

	fahrenheit = float64(cel)*1.8 + 32
	kelvin = float64(cel) + 273.15

	fmt.Printf("%d celcius = %v fahreinheit\n", cel, fahrenheit)
	fmt.Printf("%d celcius = %v kelvin\n", cel, kelvin)

}
